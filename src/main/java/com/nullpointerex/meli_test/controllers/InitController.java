package com.nullpointerex.meli_test.controllers;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.nullpointerex.meli_test.models.Satelite;

public class InitController {
	static Satelite Kenobi = new Satelite(-500, -200);
	static Satelite Skywalker = new Satelite(100, -100);
	static Satelite Sato = new Satelite(500, 100);
	
	public static boolean comprobar( double x, double y, Satelite s, double distancia){
		
		int coordx = s.getX();
		int coordy = s.getY();
		
		boolean flag = true;
		//(x-h)^2 + (y-k)^2 = r^2
		
		distancia = Math.round(distancia);
		//System.out.println(x+" - "+(coordx)+": "+(x-coordx));
		//System.out.println(y+" - "+(coordy)+": "+(y-coordy));
		//System.out.println(distancia);
		double suma = (double) (Math.pow( x-coordx,2) +  Math.pow( y-coordy ,2));
		
		if (suma != distancia)
			flag = false;
		return flag;
	}
	
	public static ArrayList<Double> circucnf(double distancias, Satelite s) { //distancia al cuadrado
		
		int h = s.getX();
		int k = s.getY();
		
		ArrayList<Double> a = new ArrayList<>();
		a.add((double)(-2*h));
		a.add((double)(-2*k));
		a.add((double)( distancias - Math.pow(h, 2) - Math.pow(k, 2)));
		//System.out.println(a);
		return a;
	}
	
	public static ArrayList<Double> ecuacion( ArrayList<Double> a, ArrayList<Double> b) {
		
		ArrayList<Double> ec = new ArrayList<>();
		
		for (int i = 0; i < 3; i++) {
			ec.add(a.get(i)- b.get(i));
		}
		return ec;
	}
	
	public static ArrayList<Double> getcargocoords( ArrayList<Double> a, ArrayList<Double> b, ArrayList<Double> c) {
		
		ArrayList<Double> res = new ArrayList<>();
		ArrayList<Double> ec1 = ecuacion(a, b);
		ArrayList<Double> ec2 = ecuacion(a, c);
		
		double a1 = ec1.get(0);
		double b1 = ec1.get(1);
		double c1 = ec1.get(2);
		
		double a2 = ec2.get(0);
		double b2 = ec2.get(1);
		double c2 = ec2.get(2);

		double denominador = (a1 * b2) - (a2 * b1);
		//si demoninador es cero no tiene solucion
		if (denominador == 0)
			return res; 
				
		
		double x = ((c1 * b2) - (c2 * b1)) / denominador;
		double y = ((a1 * c2) - (a2 * c1)) / denominador;
		
		res.add(x);
		res.add(y);
		
		return res;
	}
	
	public static double[] GetLocation(double[] distances) {
		//d1 450000, d2 40000, d3 160000
		double coords[] = new double[2];
			
		
		
		distances[0] = (double) Math.pow(distances[0], 2);
		distances[1] = (double) Math.pow(distances[1], 2);
		distances[2] = (double) Math.pow(distances[2], 2);
		
		ArrayList<Double> kenobi  = circucnf( distances[0], Kenobi);
		ArrayList<Double> skywalker = circucnf( distances[1], Skywalker);
		ArrayList<Double> sato = circucnf( distances[2], Sato);

		ArrayList<Double> carguero = getcargocoords(kenobi, skywalker, sato);

		boolean flag1 = (comprobar( Math.round(carguero.get(0)), Math.round(carguero.get(1)), Kenobi, distances[0]));
		boolean flag2 = (comprobar( Math.round(carguero.get(0)), Math.round(carguero.get(1)), Skywalker, distances[1]));
		boolean flag3 = (comprobar( Math.round(carguero.get(0)), Math.round(carguero.get(1)), Sato, distances[2]));
		
		if ((flag1 == true) && (flag2 == true) && (flag3 == true)) {
			coords[0] = Math.round(carguero.get(0));
			coords[1] = Math.round(carguero.get(1));
			return coords;
		}
		else
			throw new ResponseStatusException(
					   HttpStatus.NOT_FOUND, "not found"
					 );

		
	}
	
	public static String GetMessage(String[][] messages){
		
		String[] result = messages[0];
		String ret = new String();
		
		for (int i = 0; i< messages.length; i++) {
			for (int j = 0; j< messages[i].length; j++) {
				if (!( messages[i][j].equals(""))){
					if (!(messages[i][j].equals(result[j])) && !(result[j].equals("")))
						throw new ResponseStatusException(
								   HttpStatus.NOT_FOUND, "not found"
								 );
					else
						result[j] = messages[i][j];
				}
				}
				
			}
			
		
		for (int i = 0; i< result.length; i++) {
			ret += result[i]+" ";
		}
			return ret.trim();
			
		}
}