package com.nullpointerex.meli_test.controllers;

import com.nullpointerex.meli_test.controllers.InitController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.json.JSONArray;
import org.json.JSONObject;

@Controller
@RequestMapping("/topsecret")
public class ComunicationController {

   @ResponseBody
   @RequestMapping(method = POST, produces = "application/json")   
   public String topsecret(
		   @RequestBody String payload
		   ) {
	   double[] distancias = new double[3];
	   double[] dist= new double[3];
	   
	   String[][] mensajes = new String[3][];
	   
	   
	   JSONObject jstr = new JSONObject(payload);
	   
	   
	   
	   JSONArray array = jstr.getJSONArray("satellites");
	   
	   
	  for(int i=0; i < array.length(); i++){
		  JSONObject object = array.getJSONObject(i);
		  //System.out.print(object.get("message")+" ");
		  distancias[i] = object.getDouble("distance");
		  JSONArray arrJson = object.getJSONArray("message");
		  
		  String[] arr = new String[arrJson.length()];
		  
		  for( int j = 0; j< arrJson.length(); j++) {
			  arr[j] =  arrJson.getString(j);
		  	}
		  mensajes[i] = arr; 
		  }
	   
	   
	   dist = InitController.GetLocation(distancias);
	   
	   JSONObject jo = new JSONObject();
	   JSONObject position = new JSONObject();
	   
	   position.put("x", dist[0]);
	   position.put("y", dist[1]);
	   jo.put("position", position);
	   jo.put("message", InitController.GetMessage( mensajes));
	   
	   
	   return jo.toString();
   }

}