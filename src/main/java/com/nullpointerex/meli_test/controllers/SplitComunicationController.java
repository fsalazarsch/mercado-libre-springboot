package com.nullpointerex.meli_test.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nullpointerex.meli_test.controllers.InitController;
import com.nullpointerex.meli_test.models.Satelite;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.json.JSONArray;
import org.json.JSONObject;

@Controller
@RequestMapping("/topsecret_split/{satelite}")
public class SplitComunicationController {
	
   @ResponseBody
   @RequestMapping(method = GET, produces = "application/json")
   public String demo(
		   @RequestBody String payload,
		   @RequestParam(required = false) String satelite
		   ) {
	   

	   double[] distancias = new double[3];
	   double[] dist= new double[3];
	   String[][] mensajes = new String[3][];
	   
	   JSONArray array = new JSONArray(payload);
	   
	   
	  for(int i=0; i < array.length(); i++){
		  JSONObject object = array.getJSONObject(i);
		  //System.out.print(object.get("message")+" ");
		  distancias[i] = object.getDouble("distance");
		  JSONArray arrJson = object.getJSONArray("message");
		  
		  String[] arr = new String[arrJson.length()];
		  
		  for( int j = 0; j< arrJson.length(); j++) {
			  arr[j] =  arrJson.getString(j);
		  	}
		  mensajes[i] = arr; 
		  }
	   
	   
	   dist = InitController.GetLocation(distancias);
	   
	   JSONObject jo = new JSONObject();
	   JSONObject position = new JSONObject();
	   
	   position.put("x", dist[0]);
	   position.put("y", dist[1]);
	   jo.put("position", position);
	   jo.put("message", InitController.GetMessage(mensajes));
	   
	   
	   return jo.toString();
   }

   @ResponseBody
   @RequestMapping(method = POST, produces = "application/json")   
   public String topsecret( //devuelve la posicion del satelite
		   @PathVariable("satelite") String satelite_name
		   ) {
	   
	   Satelite item = null;
	   
	   if (satelite_name.equals("kenobi"))
		   item = InitController.Kenobi;
	   if (satelite_name.equals("skywalker"))
		   item = InitController.Skywalker;
	   if (satelite_name.equals("sato"))
		   item = InitController.Sato;
	   

	   
		JSONObject position = new JSONObject();
	   position.put("x", item.getX());
	   position.put("y", item.getY());
	   
	   
	   return position.toString();
   }

}