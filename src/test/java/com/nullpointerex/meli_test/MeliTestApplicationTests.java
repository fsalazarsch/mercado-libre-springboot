package com.nullpointerex.meli_test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.nullpointerex.meli_test.controllers.InitController;
import com.nullpointerex.meli_test.models.Satelite;

@SpringBootTest
class MeliTestApplicationTests {
	
	public InitController initcontroller;
	public Satelite Kenobi;
	public Satelite Skywalker;
	public Satelite Sato;
	
	@BeforeEach
    void setUp() throws Exception {
        initcontroller = new InitController();
        Kenobi = new Satelite(-500, -200);
    	Skywalker = new Satelite(100, -100);
    	Sato = new Satelite(500, 100);
    }

	@Test
	void contextLoads() {
	}

	@Test                                               
    @DisplayName("Comparacion resultado")   
    public void testComprobar() {
        assertTrue(InitController.comprobar(100, 100, Kenobi, Math.pow(670.82039325, 2)),
        		"Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");          

        assertTrue(InitController.comprobar(100, 100, Skywalker, Math.pow(200.0, 2)),
        		"Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");          

		assertTrue(InitController.comprobar(100, 100, Sato, Math.pow(400.0, 2)),      
		        "Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");          

        assertFalse(InitController.comprobar(-100, 75.5, Kenobi, Math.pow(100, 2)),      
                "Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");          

        assertFalse(InitController.comprobar(-100, 75.5, Skywalker, Math.pow(115.5, 2)),
        		"Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");          

        assertFalse(InitController.comprobar(-100, 75.5, Sato, Math.pow(142.7, 2)),      
        "Comprobado con funcion de circunferencia (x-h)^2 + (y-k)^2 = r^2");  
	}

	@Test                                               
    @DisplayName("Crear funcion circunferencia")   
    public void testCircucnf() {
		ArrayList<Double> t = new ArrayList<Double>();
		
		t.add((double) 1000 );
		t.add((double) 400 );
		t.add( (double) 160000 );
		
		assertEquals(t, InitController.circucnf( 450000, Kenobi));
		t.clear();
		
		t.add((double) -200 );
		t.add((double) 200 );
		t.add( (double) 20000 );
		
		assertEquals(t, InitController.circucnf( 40000, Skywalker));
		t.clear();
		
		t.add((double) -1000 );
		t.add((double) -200 );
		t.add( (double) -100000 );
		
		assertEquals(t, InitController.circucnf( 160000, Sato));

		t.clear();
		
		t.add((double) 1000 );
		t.add((double) 400 );
		t.add( (double) -60000 );
		
		assertNotEquals(t, InitController.circucnf( 10000, Kenobi));
		
	}
	
	@Test                                               
    @DisplayName("Crear ecuacion")   
    public void testEcuacion() {
		ArrayList<Double> a = new ArrayList<Double>();
		a.add((double) 10 );
		a.add((double) 4 );
		a.add( (double) 16 );
		
		
		ArrayList<Double> b = new ArrayList<Double>();
		b.add((double) -2 );
		b.add((double) 2 );
		b.add( (double) 2 );
		
		ArrayList<Double> c = new ArrayList<Double>();
		c.add((double) -10 );
		c.add((double) -2 );
		c.add( (double) -10 );
		
		ArrayList<Double> x = new ArrayList<Double>();
		x.add((double) 12 );
		x.add((double) 2 );
		x.add( (double) 14 );
		
		assertEquals(x, InitController.ecuacion(a, b));
		
		x.clear();
		x.add((double) 20 );
		x.add((double) 6 );
		x.add( (double) 26 );
		
		assertEquals(x, InitController.ecuacion(a, c));
		
	}

	@Test                                               
    @DisplayName("Get coordenadas carguero")   
    public void testgetcargocoords() {
		ArrayList<Double> a = new ArrayList<Double>();
		ArrayList<Double> b = new ArrayList<Double>();
		ArrayList<Double> c = new ArrayList<Double>();
		ArrayList<Double> t = new ArrayList<Double>();
		
		a.add((double) 1000 );
		a.add((double) 400 );
		a.add( (double) 160000 );		
		b.add((double) -200 );
		b.add((double) 200 );
		b.add( (double) 20000 );
		c.add((double) -1000 );
		c.add((double) -200 );
		c.add( (double) -100000 );

		t.add((double) 100 );
		t.add((double) 100 );
		
		assertEquals(t, InitController.getcargocoords(a, b, c), "sistema con solucion");

		a.clear();
		b.clear();
		c.clear();
		t.clear();
		
		a.add((double) 1); 
		a.add((double) 1 );
		a.add( (double) 1);		
		b.add((double) 0);
		b.add((double) 0);
		b.add( (double) 0);
		c.add((double) -2 );
		c.add((double) -2);
		c.add( (double) -2);
		assertEquals(t, InitController.getcargocoords(a, b, c), "sistema sin solucion");

	}

	@Test                                               
    @DisplayName("GetLocation")   
    public void testGetLocation() {
		double[] coords = {100.0, 100.0};
		double[] distances = {Math.sqrt(450000.0), 200.0, 400.0};
		assertArrayEquals(coords, InitController.GetLocation(distances));
		
		double[] coords2 = {-100.0, 75.5};
		double[] distances2 = {100.0, 115.5, 142.7};
		
		Exception exception = assertThrows(ResponseStatusException.class, () ->
		InitController.GetLocation(distances2));
		assertEquals("404 NOT_FOUND \"not found\"", exception.getMessage());		
	}

	@Test                                               
    @DisplayName("GetMessage")   
    public void testGetMessage() {
		
		String[] mensaje1 = {"", "have", "", "bad", "feeling", "", ""};
		String[] mensaje2 = {"", "", "a", "bad", "feeling", "about", ""};
		String[] mensaje3 = {"I", "have", "a", "", "feeling", "", "this!"};
		String[] mensaje4 = {"May", "", "4", "", "with", "you"};
		   
		String[][] mensajes = {mensaje1, mensaje2, mensaje3};
		
		assertEquals("I have a bad feeling about this!", InitController.GetMessage(mensajes));
		
		String[][] mensajes2 = {mensaje1, mensaje2, mensaje4};
		
		Exception exception = assertThrows(ResponseStatusException.class, () ->
		InitController.GetMessage(mensajes2));
		assertEquals("404 NOT_FOUND \"not found\"", exception.getMessage());		
	}
	
}
