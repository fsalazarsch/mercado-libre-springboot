# README #

Prueba Fuego de Quasar para MeLi

### Logica detras del desarrollo ###

* Usando diagrama de trilateracion, se ontiene que los 3 radios de los satelites tendran un punto en comun
* Esos 3 satelites bajo el plano cartesiando derivaran en una formula de circunferencia de radio (h,k)
* El punto en comun es la solucion matematica de esas 3 ecuaciones
* Restando 2 de ellas se logra un sistema de ecuacion simple, que se calcula con determinantes
	- Hay casos en los que se logra matematicamente la solucion, pero esta al comprobarla con la funcion original da una solucion incongruente
* Si las coordenadas se corresponden con las 3 ecuaciones de circunferencia, la solucion es congruente 

### Como instalarlo? ###

* Clona este repositorio
* En la terminal: 'mvn spring-boot:run'
	- Se instalaran todos los paquetes y se creara el jar para ser ejecutado
* Puerto por defecto: 8080

### Implementaciones en vivo ###

* Url AWS: 3.140.123.112:8080
* Swagger: http://3.140.123.112:8080/swagger-ui.html
* Testeado con Sonarqube en local
